import Ember from 'ember';
import layout from '../templates/components/workout-row';

export default Ember.Component.extend({
  layout: layout,
  tagName: 'li',
  isEditing: false,
  store: null,
  day: null,
  actions: {
    edit: function(){
      this.set('isEditing', true);
    },
    cancelEdit: function(){
      this.set('isEditing', false);
    },
    saveWorkout: function(){

      //var store = this.get('store');

      //var userdata = store.find('userdata', {day: 2, set: 1});
      //console.log(userdata);

    }
  }
});
