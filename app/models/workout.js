import DS from 'ember-data';

var Model = DS.Model.extend({
  sets: DS.attr('number'),
  reps: DS.attr('number'),
  note: DS.attr('string'),
  dropSet: DS.attr('boolean'),
  exercise: DS.belongsTo('exercise', {async: true})
});

export default Model;