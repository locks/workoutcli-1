import DS from 'ember-data';

var Model = DS.Model.extend({
  days: DS.hasMany('day', {async: true})
});

export default Model;